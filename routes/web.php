<?php

use Dedermus\EnvManager\Http\Controllers\EnvManagerController;

Route::resource('env-manager', EnvManagerController::class);
